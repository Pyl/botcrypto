const request = require('request');
const fs = require('fs');
const moment = require('moment');

//let start_date = 1483228800*1000  //Jan 01 2017
//let start_date = 1483228800  //Jan 01 2017
let start_date = 1514767500  //Jan 01 2018

//const pair = 'ETHBTC'
const timeframe = 30;
const timeperiod = 'm';
const limit = 100;
let incr = 1;
let final_data = [];
//const pairsArray = ['LTCBTC','ETHBTC','XLMBTC'];
const pairsArray = ['BTCUSD'];

console.log('TimeFrame', timeframe);
console.log('Start Date', moment.unix(start_date).format());

//throw new Error('Stop');

for(let pair in pairsArray){
    callExchangeHistory(pairsArray[pair],start_date, incr, final_data);
}

//callExchangeHistory(start_date, incr, final_data)


function callExchangeHistory(pair, start_date, incr, final_data){

    let url = 'https://api.bitfinex.com/v2/candles/trade:'+timeframe+timeperiod+':t'+pair+'/hist?sort=-1&limit='+limit+'&start='+start_date;

    request.get(url, (error, response, data ) => {
           
        if(data.length == 36 ) {
            return Promise.reject('Limite atteinte');
        }

        temp_data = JSON.parse(data);

             
        start_date = temp_data[temp_data.length-1][0]+timeframe*60*1000;
        //let humanDate = moment.unix(start_date/1000).format()
        
        for(let candle in temp_data) {
            temp_data[candle][0] = moment.unix(temp_data[candle][0]/1000).format();         
        }
        
        
        final_data = final_data.concat(temp_data);
        
        if(incr < 10) {            
            setTimeout(()=>{
                console.log(new Date(), pair, final_data.length);
                incr++;
                callExchangeHistory(pair, start_date, incr, final_data);
            },3000*pairsArray.length);
        }

        fs.writeFileSync('./Datasets/'+pair+'-'+timeframe+timeperiod+'.json',  JSON.stringify(final_data));
    });     
}