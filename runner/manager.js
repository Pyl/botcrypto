"use strict";

const fs = require('fs');
const SMA = require('technicalindicators').SMA;

const Manager =  {

    pairsArray : ['ETHBTC','LTCBTC'],
    maPeriods : 20,
    

    /*
    for(pair in pairsArray) {
        pairs[pair] = {
            ma: new SMA({period:this.maPeriods, values:[] }),
        }
    }
    */
    
    init () {
        let pairs = {};
        for(pair in this.pairsArray) {
            pairs[pair] = {
                ma: new SMA({period:this.maPeriods, values:[] }),
            }
        }
    },

    /**
     * function RUN
     */
    runBot (){
        this.init();
        console.log('lancer runBot');
    }
}

module.exports = Manager;