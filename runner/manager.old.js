
const fs = require('fs');
const SMA = require('technicalindicators').SMA;
const ADX = require('technicalindicators').ADX;
const BfxTrade = require('./BfxTrade');
const moment = require('moment');

const CLOSE = 2;
const HIGH = 3;
const LOW = 4;
const VOLUME = 5;

class Manager {

    constructor(timeframe=1,pairsArray){
        
        this.timeframe = timeframe;
        this.pairsArray = pairsArray;
        this.maPeriods = 20;
        this.adxPeriod = 14;
        this.trendStrength = 25;
        this.pairs = [];
        this.marketData = {};
        this.nbLongPosition = 0;
        this.nbShortPosition = 0;
        this.openedPosition = 0;
        this.nbTrade = 0;
        this.bfx = new BfxTrade();
        this.succes = 0;
        this.loss = 0;
        this.init(this.pairsArray);
    };

     init(pairsArray) {
        for(let pair in pairsArray) {
            this.pairs[pairsArray[pair]] = {
                ma: new SMA({period:this.maPeriods, values:[] }),
                adx: new ADX({period:this.adxPeriod, close:[], high:[], low:[]}),
                maValue : 0,
                adxVAlue: 0,
                prevMaValue : 0,
                prevClose : 0,
                long: false,
                short: false,
                stopLossPrice:0,
                entryAmount:0,
                entryPrice:0,
            }
        }
    };

    runBot(){
        //console.log(this.marketData);
        let compareMarketLength = 0;
        for(let pair in this.pairsArray) {
            console.log(this.timeframe);
            try {
                let crypto = this.pairsArray[pair];
                this.marketData[crypto] = JSON.parse(fs.readFileSync(__dirname+'/../Datasets/'+crypto+'-'+this.timeframe+'m.json','utf8'));
                console.log(this.pairsArray[pair], this.marketData[crypto].length)

                if(compareMarketLength == 0){
                    compareMarketLength = this.marketData[crypto].length;
                }else{
                    if(compareMarketLength !== this.marketData[crypto].length){
                        return Promise.reject('Les marchés ne sont pas de la même taille, relancer le dataset setup ou changer de marché');
                    }
                }

            } catch (error) {
                console.log('Fichier de la pair : '+this.pairsArray[pair]+' non trouvé ');
            }
            
        }

        for(let i=0;i<this.marketData[this.pairsArray[0]].length;i++){
            for(let pair in this.marketData){
                this.updateIndicator(pair, this.marketData[pair][i]);
                //console.log('Timestamp',this.marketData[pair][i][0]/1000);
                //console.log('DATE', moment.unix(this.marketData[pair][i][0]/1000).format("MMMM Do YYYY, h:mm:ss a"));
            }
        }
        /*
        for ( let pair in this.marketData) {
            for(let candle in this.marketData[pair]){
                candle = this.marketData[pair][candle];
                this.calculateMA(pair, candle[CLOSE]);
            }
        }
        */
    };

    updateIndicator(pair, price) {
        this.pairs[pair]['maValue'] = this.pairs[pair]['ma'].nextValue(price[CLOSE]);
        this.pairs[pair]['adxValue'] = this.pairs[pair]['adx'].nextValue({close: price[CLOSE], high:price[HIGH], low:price[LOW]});
        //console.log(pair, this.pairs[pair]['maValue']);

        this.findTradeOpportunity(pair, price[CLOSE]);
        this.pairs[pair]['prevMaValue'] = this.pairs[pair]['maValue']
        this.pairs[pair]['prevClose'] = price[CLOSE]
    };

    /*
    calculateMA(pair, close) {
        this.pairs[pair]['maValue'] = this.pairs[pair]['ma'].nextValue(close);
        //console.log(pair, this.pairs[pair]['maValue']);

        this.findTradeOpportunity(pair, close);
        this.pairs[pair]['prevMaValue'] = this.pairs[pair]['maValue']
        this.pairs[pair]['prevClose'] = close
    };
    */


    findTradeOpportunity(pair, close){
        if(!this.pairs[pair]['long'] && !this.pairs[pair]['short']) // si aucune position
        {
            if(this.pairs[pair]['adxValue'] > this.trendStrength){
                console.log(this.pairs[pair]['adxValue']);
            }
            if(this.pairs[pair]['prevClose'] < this.pairs[pair]['prevMaValue'] && close > this.pairs[pair]['maValue'] && this.pairs[pair]['adxValue'] > this.trendStrength )
            {
                this.openLongPosition(pair, close);
            }
            else if(this.pairs[pair]['prevClose'] > this.pairs[pair]['prevMaValue'] && close < this.pairs[pair]['maValue'])
            {
                this.openShortPosition(pair, close);
            }
        }
        else if(this.pairs[pair]['long']) // Si en position acheteur
        {
            if(close < this.pairs[pair]['maValue'] && close > this.pairs[pair]['entryPrice']*1.004)
            {
                this.succes++;
                this.closeLongPosition(pair, close);
                
            }
            else if(close < this.pairs[pair]['stopLossPrice'])
            {
                this.loss++;
                this.closeLongPosition(pair, this.pairs[pair]['stopLossPrice']);
                
            }
        }
        else if(this.pairs[pair]['short']) // Si en position vendeur
        {
            if(close > this.pairs[pair]['maValue'] && close < this.pairs[pair]['entryPrice']*0.996)
            {
                this.succes++;
                this.closeShortPosition(pair, close);
                
            }
            else if(close > this.pairs[pair]['stopLossPrice'])
            {
                this.loss++;
                this.closeShortPosition(pair, this.pairs[pair]['stopLossPrice']);
                
            }
        }else{
            console.log('AUCUN TRADE');
        }
    }

    //LONG POSITION

    openLongPosition(pair, close){
        this.pairs[pair]['stopLossPrice'] = close*0.98;
        this.pairs[pair]['entryAmount'] = this.getPositionSize(close);
        this.pairs[pair]['entryPrice'] = close;
        this.orderLongPosition(pair, close, 'buy', 'long', 'OUVERTURE LONG POSITION a ', true, 1);
        /*
        let self = this;
        this.bfx.testTrade(pair, close, this.pairs[pair]['entryAmount'], 'buy', function()
        {
            self.pairs[pair]['long'] = true;
            self.nbLongPosition++;
            self.nbTrade++;
            console.log(pair, 'OUVERTURE LONG POSITION a ', close);
            console.log(pair, 'Stop loss price ', self.pairs[pair]['stopLossPrice']);
        });
        */        
    }

    closeLongPosition(pair, close){

        this.orderLongPosition(pair, close, 'sell', 'long', 'CLOSE LONG POSITION a ', false, -1);
        this.pairs[pair]['entryAmount'] = 0;
        this.pairs[pair]['entryPrice'] = 0;
        this.pairs[pair]['stopLossPrice'] = 0;
        /*
        let self = this;

        this.bfx.testTrade(pair, close, this.pairs[pair]['entryAmount'], 'sell', function()
        {
            self.pairs[pair]['long'] = false;
            self.nbLongPosition--;
            self.nbTrade++;
            self.pairs[pair]['entryAmount'] = 0;
            console.log(pair, 'CLOSE LONG POSITION a ', close);
            console.log(pair, 'Result amount ', self.bfx.initAmount);
        });
        */
       console.log(pair, 'Result amount ', this.bfx.initAmount);
    }

    //SHORT POSITION

    openShortPosition(pair, close){
        this.pairs[pair]['stopLossPrice'] = close*1.02;
        this.pairs[pair]['entryAmount'] = this.getPositionSize(close);
        this.pairs[pair]['entryPrice'] = close;

        this.orderShortPosition(pair, close, 'sell', 'short', 'OUVERTURE SHORT POSITION a ', true, 1);

        /*
        let self = this;
        this.bfx.testTrade(pair, close, this.pairs[pair]['entryAmount'], 'sell', function()
        {
            self.pairs[pair]['short'] = true;
            self.nbShortPosition++;
            self.nbTrade++;
            console.log(pair, 'OUVERTURE SHORT POSITION a ', close);
            console.log(pair, 'Stop loss price ', self.pairs[pair]['stopLossPrice'])
        });
        */
            
    }

    closeShortPosition(pair, close){

        this.orderShortPosition(pair, close, 'buy', 'short', 'CLOSE SHORT POSITION a ', false, -1);
        this.pairs[pair]['entryAmount'] = 0;
        this.pairs[pair]['entryPrice'] = 0;
        this.pairs[pair]['stopLossPrice'] = 0;
       console.log(pair, 'Result amount ', this.bfx.initAmount);

        
        /*
        let self = this;
        this.bfx.testTrade(pair, close, this.pairs[pair]['entryAmount'], 'buy', function()
        {
            self.pairs[pair]['short'] = false;
            self.pairs[pair]['entryAmount'] = 0;
            self.nbShortPosition--;
            self.nbTrade++;
            console.log(pair, 'CLOSE SHORT POSITION a', close);
            console.log(pair, 'Result amount ', self.bfx.initAmount);
        });
        */
        
    }



    orderLongPosition(pair, close, type, order, descriptionOrder, pairsLong, incrLongPosition, ){
        let self = this;
        this.bfx.testTrade(pair, close, this.pairs[pair]['entryAmount'], type, order, function()
        {
            
            self.pairs[pair]['long'] = pairsLong;
            self.nbLongPosition = self.nbLongPosition + incrLongPosition;
            self.nbTrade++;
            self.describe(pair, descriptionOrder, close);
        });
    }

    orderShortPosition(pair, close, type, order, descriptionOrder, pairsShort, incrShortPosition, ){
        let self = this;
        this.bfx.testTrade(pair, close, this.pairs[pair]['entryAmount'], type, order, function()
        {
            
            self.pairs[pair]['short'] = pairsShort;
            self.nbShortPosition = self.nbShortPosition + incrShortPosition;
            self.nbTrade++;
            self.describe(pair, descriptionOrder, close);
        });
    }

    getPositionSize(close){
        return (this.bfx.initAmount/(this.pairsArray.length - (this.nbLongPosition+this.nbShortPosition)) / close ) ;
    }



    //Print or console function

    describe(pair, orderPosition, close){
        console.log(pair, orderPosition, close, 'Amount : ', this.pairs[pair]['entryAmount']);
        //console.log(pair, 'Stop loss price ', this.pairs[pair]['stopLossPrice']);
        console.log('Open Long : ',this.nbLongPosition,' Open Short : ', this.nbShortPosition);
        //console.log(pair, 'Result amount ', this.bfx.initAmount);
        console.log('----------------------------------------------------------------------');
    }

    print(){
        console.log('Position long ouverte : '+this.nbLongPosition);
        console.log('Position short ouverte : '+this.nbShortPosition);
        console.log('Nombre de trade effectués : '+this.nbTrade);
        console.log('Trades reussi : ',this.succes);
        console.log('Trades perdu : ',this.loss);
        console.log('Total Amount : ',this.bfx.initAmount);
    }
};

module.exports = Manager;